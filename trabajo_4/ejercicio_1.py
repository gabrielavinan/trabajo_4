#___author___ "Gabriela Viñan"
#___ email___ gabriela.vinan@unl.edu.ec
#Escribe un programa que lea repetidamente números hasta que el usuario introduzca “fin”. Una vez se haya
#introducido “fin”,muestra por pantalla el total, la cantidad de números y la media de esos números.
contador=0
suma=0
while True:
    numero = input("Introduce un número (o 'fin' para terminar): ")
    if numero == "fin":
        break
    try:
        suma = suma + int(numero)
        contador= contador + 1

    except ValueError:
        print("Numero introducido incorrecto. Ingrese nuevamente...")

print("La suma de los números es: ", suma)
print("La cantidad de números introducidos es : ", contador)
print("El promedio de valores es:  ", suma / contador)