#___author___ "Gabriela Viñan"
#___ email___ gabriela.vinan@unl.edu.ec
#Escribe otro programa que pida una lista de números como la anterior y al final muestre por pantalla el
#máximo y mínimo de los números, en vez de la media.
contador=0
suma=0
maximo=0
minimo=0
while True:
    numero = input("Introduce un número (o 'fin' para terminar): ")
    if numero == "fin":
        break
    try:
        suma = suma + int(numero)
        contador= contador + 1
        valor = int(numero)
        if valor > maximo:
            maximo = valor
        if valor < minimo:
            minimo = valor
        elif contador == 1:
                minimo = valor

    except ValueError:
        print("Numero introducido incorrecto. Ingrese nuevamente...")

print("La suma de los números es: ", suma)
print("La cantidad de números introducidos es : ", contador)
print("El numero maximo es :  ", maximo)
print("El numero minimo es :  ", minimo)